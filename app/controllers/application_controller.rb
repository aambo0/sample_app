class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
 before_action :set_locale
 
  
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    def set_locale
    
      # set locale to the locale on the request params or the default set in the config
     locale = params[:locale].to_s.strip.to_sym
     I18n.locale = I18n.available_locales.include?(locale) ? locale : I18n.default_locale
      
      # @local_dir instance variable to pass to the view for right-to-left and left-to-right languages
      case I18n.locale
      when :fa, :ar
        @local_dir = "rtl"
      else
        @local_dir = "ltr"
      end
    end  
    # avoids having to add the locale to every link_to 
    def default_url_options
      { locale: I18n.locale }
    end
end