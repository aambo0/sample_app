class CreateUserTranslations < ActiveRecord::Migration[5.1]
      def up
        User.create_translation_table!(
          { name: :string},
          { migrate_data: true }
        )
      end
    
      def down
        User.drop_translation_table! migrate_data: true
      end
end