class CreateDogs < ActiveRecord::Migration[5.1]
  def change
    create_table :dogs do |t|
      t.text :name
      t.datetime :vaccine
      t.references :user, foreign_key: true

      t.timestamps
    end
        add_index :dogs, [:user_id, :created_at]

  end
end
